## 2012-12-31

* 진행 요약
    - git 의 기본 개념

* 명령어 정리
    - git init
    - git status
    - git add
    - git commit 
        - git commit -m
        - git commit -am
        - git commit --amend -C HEAD
    - git clone 
    - git log
    - git pull

* 과제 
    - pro.git 1-2장
    - bitbucket.org: jspatterns clone하기 
    - git 명령어에 익숙해지기
        - pull 등을 직접해보기 
    
