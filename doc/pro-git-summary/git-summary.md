Git Again
==========

## Git의 저장 구조

- working : stage : repo
	- git diff: stage <> working
		- git diff --cached: staged <> repo
	- git reset: repo > stage
	- git checkout:
		1. repo > stage + working
			- /
		2. stage > working: 
			- git checkout -- hello.c
- 파일 내용으로만 저장(BLOB)
	- rename dectect 테스트
	- git mv == git rm + git add

## 로그

- git log 
	- 현재 branch only
- git diff
	- stage와 working dir
	- 두 branch를 비교할 경우. diff 결과에서는 상대 브랜치에 추가된 내용은 현재 브랜치에서 삭제하려는 것으로 표시된다.

## commit 관련

- 공백 문자 정리
	- git diff --check
- 커밋 메세지 형식 
	- 헤더 + 본문(목록 사용 허용)
- 일부만 커밋하기
    - [관련 stackoverflow 링크](http://goo.gl/r48Pl)
    - git add -p filename 또는 
    - git add --interactive filename
    - ?로 help 조회

## stage 관리

* git rm --cached
	- git rm: stage와 working dir **모두**에서  삭제하는 명령어
	- working dir에 있는 내용은 삭제하고 stage에 있는 내용(cached)만 삭제
* git diff --cached file-name
	- stage <> repo

## branch 이동

* git checkout
	- 변경 내용이 있을 경우: stash를 이용해 쟁여놓고 이동

## 원격지 관리 

- git clone 
	- git remote origin을 생성한다.	
	- clone을 했을 경우 branch가 모두 working dir에 보이지는 않는다
	- git checkout -b master origin/master를 이용한다.

- git checkout 
	1. 저장소 > Stage 영역 + Working Dir
		- git checkout master~2
	2. Stage 영역 > Working Dir
	    - git checkout -- hello.c
	    - '--' 생략 가능
- git fsck 
	- `git fsck --lost-found`: staged file에서 복구함 
		- **Need Test**

- git remote
	- 원격 브랜치 삭제하기
		- git push origin :branch_to_delete
	- Branch 목록 조회
		- git branch -r

- 원격지 브랜치  
	- 원격지 브랜치를 가져와 작업
		- 원격 미완 브랜치 완성 
	- 원격 merge 후 fetch 작업

- rebase
	- 과정: 현재 브랜치를 싸들고(patch) 이사가서 대상 브랜치에 적용
		1. 공통 커밋부터 "현재 브랜치"의 patch를 만들어 대상 브랜치에 적용
		2. 적용 후 현재 브랜치 및 HEAD를 최근 patch로 위치
		3. 대상 branch는 이전 위치에 남아있다.
		  	- 이제 대상 branch에서 fast-foward 할 수 있다.
	- Rebase와 Merge는 결과적으로 같지만, commit history가 다르다.

	- server에서 파생한 client 브랜치만 master에 반영
		- git rebase --onto master server client
		- server와 client의 공통 조상 이후의 패치 생성 후 master에 적용
		- server와의 공통 조상 이후 **client의 패치**가 적용됨

##  커밋 찾아내기

* git show
	- git show HEAD@{5}: HEAD 5번 이전 
	- git show master@{yesterday}: 어제 master

* git log
	- git log master..file1: 
		- file1에서는 도달 가능, master에서는 도달 불가 커밋
			- file1 - master 의 빼기 개념
		- 현재 브랜치에만 있는 커밋 조회:
			- `git log origin/master .. HEAD`

	- git log --no-merges origin/master ^issue54
		- --no-merges 와 --max-parents=1은 같다
	- git log origin/feature1 ^feature1
		- origing/feature1에서 현재 feature1 브랜치에 없는 커밋(?)
		- --not feature1을 써도 된다.
	- 변경 내용(diff) 
		- git log -p 

* 공통 부모(merge-base) 관련 명령
	- git merge-base
		- 공통 부모 찾기
		- git checkout -b hotfix <merge-base commit> 으로 hotfix 생성 가능
	- branch1...master
		- 두 브랜치 공통 영역을 제외함
		- 예시 
			1. git diff master...branch1
				- '두 브랜치의 공통 부모'와 '현재 브랜치'를 비교함
			2. git log --left-right master...experiment
				-  두 브랜치의 공통 영역을 제외하고 표시(왼쪽 오른쪽 소속 표시)

## merge/rebase/check-pick 기본 

- 최신 변경 내용 가져오기
	- git fetch origin

- merge
	- subversion은 서로 다른 파일의 merge는 서버사이드에서 처리
	- 도달 가능/불가 commit이 존재한다면, 로컬에서 처리해야 한다.
	- merge commit 은 merge 작업이 이루어진 branch에 속한다. 

- rebase
- cherry-pick
	- 하나의 commit만 rebase하는 개념
	- commit을 골라야 할 때, 토픽 브랜치에 commit이 1개일 때
	- 해당 commit의 patch를 만들어 **현재 브랜치**에 적용
		- 적용 시점이 달라 sha1은 달라짐
	- git cherry-pick target_commit
	- git 저장소는 delta storage가 아니므로 cmt1 <- cmt2 이고 cmt2를 cherry-pick하면 cmt2 - cmt1의 내용이 반영되는 것이 아니다. (확인 요망)

- 커밋 가이드라인
	- 공백 문자 정리: git diff --check
	
			git diff --check                                                                     
			test.txt:2: trailing whitespace.

- 원격지 반영
	-git push origin local:server	
		- local 브랜치 > server 브랜치로 push
	- git request-pull
		- pull request용 메시지 작성
		- git request-pull arg1 arg2
			1. arg1: ??
			2. arg2: ??

## patch 사용하기

- 기여하는 사람이 가끔/일시적으로 기여하는 경우엔 patch가 낫다
- git format-patch -M target_branch
	- 대상 브랜치와의 공통 부모 부터 patch 생성
	- M옵션: 파일명 변경 검사
	- 명령 수행 후 000xx-...patch 생성됨
	- diff 보다는 format-patch 사용 권장

- 적용하기
	1. git apply
		- all or nothing
		- git apply --check로 미리 확인 가능
		- 별도 commit 필요 
	2. git am

- imap으로 메일 보내기     - .gitconfig imap

## 일부 내용만 Staging

- git add -i 사용
	- 5.[p]atch

## 커밋 정리하기

- reflog 
	- 브랜치 및 HEAD가 몇 달 동안에 가리켰던 커밋을 기록한 로그

- git rebase -i
	- 표시 순서
		- 위:아래 = 과거:최근

## Orphan 커밋 관리하기

## Stash

* 임시로 쌓아놓기
	- git stash 
		- 스택에 쌓아놈
		- git status 기준 staging 영역과 tracked modified만 보관됨
		- git status의 untracked 관리에 유의 
	- git stash list
	- git stash apply
		- git stash apply --index 를 사용하면 적용 후에도 stash 유지
		- stash 적용을 되돌릴 경우를 대비해 --index 사용은 필수
	 
	- A브랜치에서 B브랜치로 옮겨서 stash 적용도 가능함

## 기타 관리

- 소스 archiving 
	- `git archive master --prefix='project/' --format=zip > git describe master.zip`

## 보안 

- 공개키 저장
	1. gpg --list-keys
	2. gpg -a export FABC1234 | git hash-object -w --stdin
	    - 659efab... #commit hash 출력 
	3. git tag -a maintainter-pgp-pub 659efab...
	4. git show maintainer-pgp-pub | gpg --import

