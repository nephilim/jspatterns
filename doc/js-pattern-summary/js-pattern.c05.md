JS 코드 재사용 패턴 05
===================

## 객체 생성 패턴 

* namespace 함수 작성하기 

* 모듈 최상단에 로컬변수를 선언하여 의존 관계 명시하기
	
		function() {
			var host = config.server.host, 
			    suburls = config.server.urls
			//...
		} 

* private members
	- key point: 생성자 내부 변수로 선언

			exports.Gadget = function() {
				var _name = "N/A";   // _name 접근 불가
				this.getName = function() {
					return _name;
				};
				this.setName = function(name) {
					_name = name
				};
			}

		- closure 활용

	- **주의**
		1. 비공개 프로퍼티를 반환하면, 해당 프로퍼티의 수정이 가능하므로 유의한다.
			- 필요한 prop "복사" 후 반환
				- 참고: 6장 extends, extendsDeep
			- 최소 권한의 원칙(POLA)에 따를 것
 		2. 생성 시점마다 비공개 멤버가 함께 생성된다.

 	- 객체 리터럴로 private member 작성하기
 		-  function(){ } () 으로 member의 scope을 한정하면 된다
 		
 		- ** TODO: 예제 추가 **
 	- 특권 메서드 
 		- private member에 접근 권한을 가지고 있는 메서드. 
 		- 주로 closure를 이용해 private member에 접근

				var myobj = (function() {
					var _name = "nephilim";
					return {
						getName: function() {	// 특권 메서드
							return _name;
						}
					}
				}()); 				

	- 프로토타입을 이용한 비공개 멤버 설정
		- 일반적인 방식으로는 private member가 객체 생성 시 매번 생성됨  
		- prototype에 공통 (공개/비공개) 멤버를 설정함

* Prototype

	> prototype은 `function prototype`이다.

	- object의 경우 
		- 외부에서 접근 불가한 속성 __proto__을 가지고 있다.
			- var obj = { /* ... */ }; obj.protoype와 같이 접근 불가
			- 개발자 도구 등으로 확인 가능
			- __proto__은 new를 이용한 생성 시점의 Function.protoype을 가리킴

	- prototype 객체를 정의할 때는 **constructor 프로퍼티 관리에 유의**

			Obj2.prototype = new Obj1();
			Obj2.prototype.constructor = Obj2

* Constructor

	- 정의 
	> 자신을 생성한 함수를 가리킴.

		1. 일반적인 객체의 경우: 

				var obj = {};
				obj.constructor 
					> function Object() { //... }

		2. prototype 객체:
			- 자신의 생성자 함수를 가르킴

 	- 메커니즘 
 		- var obj = new Constructor();
 		- new의 역할 
 			1. **param**: this가 호출 객체를 가르킴
 			2. **return**: 생성 시 Constructor.prototype.constructor에 있는 내용(Constructor 함수)이 obj.constructor에 복사됨

	 				function Constructor() {}
	 				Constructor.prototype.constructor 
	 					> function Constructor() {} 
	 				Constructor.constructor
	 					> function Function() { [native code] }

	- prototype 객체를 정의할 때에는 constructor 프로퍼티에 신경쓴다.
		
			Obj2.prototype = new Obj1();
			Obj2.prototype.constructor = Obj2;

	- 참고

			function Constructor(){};
			var obj1 = new Constructor();
			obj1.constructor == Constructor // true
			Constructor.prototype = {}; 	// constructor=function Object(){}
			var obj2 = new Constructor();
			obj2.constuctor == Constructor;	// false

* instanceof 
	> prototype 비교를 수행함

	- __proto__와 Function.prototype을 비교함
	- 예시

			function Constructor(){};
			var obj = new Constructor();
			Constructor.prototype = {};
			[ obj instanceof Constructor, 
			  obj.constructor == Constructor, 
			  obj instanceof Object] 	// false, true, true

* revelation pattern
	- 비공개 멤버로 선언하고, 다른 이름의 공개 메서드로 노출하는 방법

			var obj;
	        (function() {
	            function func1() {/* ... */}
	            function func2() {/* ... */}
	            obj = {
	                // member1, member2, member3로 노출
	                member1: func1,	
	                member2: func2,
	                member3: func2   // 다른 노출명을 가진 동일함수
	            }
			}());

	- 참고: ECMA5에는 객체를 freeze할 수 있음

			var p = {name:"nephilim", age:30}
			p.name    	// "nephilim"
			Object.freeze(p)
			p.name = "dongwook lee" // doesn't work
			p.name 		// still "nephilim"

	- 모듈 노출 패턴
		- 모듈화 + revelation pattern

* sandbox pattern

	- 목표 코드의 형태 

			Sandbox('com', 'ysl', function(box) {
			});
	- Tip Review
		- arguments의 배열 처리: `Array.prototype.slice.call(arguments, 0)`
		- 가장 앞에 있는 원소: `array.shift()`
		- 가장 마지막에 있는 원소: `array.pop()`
		- Array 여부 검증 (?)
		- 멤버 순회하기 (?)
	- 

