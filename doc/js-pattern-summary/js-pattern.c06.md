JS 코드 재사용 패턴 06
===================

## prototype

* 참고: [Constructors considered mildly confusing](http://joost.zeekat.nl/constructors-considered-mildly-confusing.html)

1. instanceof

	[[Prototype]]과 Function.prototype을 	비교

	> in fact constructors have their own [[Prototype]] chain completely separate from the [[Prototype]] chain of > objects they initialize

	    function MyConstructor() {}
		var myobject = new MyConstructor(); 
		MyConstructor.prototype = {}; //
	    [ myobject instanceof MyConstructor,     // false !
	      myobject.constructor == MyConstructor, // true !
	      myobject instancof Object ]			 // true

	- 생성(new) 시점에 __proto__에 전달된 reference가 변경을 감지하지 못하여 링크가 유실(?)됨다

			function Parent() {
			    if(!(this instanceof Parent)) { return new Parent(); }
			    this.name = "Parent"
			}

			function Child() {
			    if(!(this instanceof Child)) {return new Child();}
			}

			var p = new Parent()
			var c = new Child()
			p.name                  // "Parent"
			c.name                  // undefined
			p.name = "Uncle"
			Child.prototype = new Parent()
			p.name                  // 'Uncle'
			c.name                  // undefined
			var c2 = new Child()
			c2.name                 // 'Parent'
			Parent.prototype.yell = function() { console.log("yell!"); }
			p.yell()
			c2.yell()               //yell!

## 상속

1. prototype 대입 패턴

		C.prototype = new P();

	- 부모의 this에 추가된 모든 멤버를 물려받는다
	- 자식 생성자 -> 부모의 생성자로 인자 전달이 안된다. 
		- new Child("James")	// Parent로 인자 전달 안됨

2. 생성자 빌려쓰기 패턴

		function Child(name) {
			Parent.apply(this, arguments)
		}

	- prototype 링크가 유지되지/부모 객체를 가리키지 않는다

			typeof Child.prototype != Parent

	- 부모의 프로퍼티를 자식에게 복사해주는 동작임
	- 다중 상속에 활용될 수 있다.

3. 1+2 모두 채용
	- 생성자 빌려쓰고, 프로토타입 지정 

			function Child(a,b) {
				Parent.apply(this, arguments);
			}
			Child.prototype = new Parent();
	- 부모의 생성자를 두 번 호출함
	- 프로퍼티를 2중으로 가지게 됨

4. prototype 공유

		C.prototype = P.prototype

	- 원칙적으로 재사용될 멤버는 this가 아닌 prototype에 저장하는 것이 맞음 
	- 문제점
		- 자식이 prototype을 수정할 경우 부모에게도 영향을 미침

5. 임시 생성자: 부모 prototype 복제
	
		function inherit(Child, Parent) {
			// Parent의 멤버는 상속에서 제외하기 위해 clean function 생성
			var Temp = function() {}	
			Temp.prototype = Parent.prototype;
			Child.prototype = new Temp();
		}

		function inherit(C, P) {
			var F = function() {};
			F.prototype = P.prototype
			C.prototype = new F();
			C.prototype.constructor = C;\
		}
	- 부모의 this에 추가된 member는 제거하고, prototype만 공유하는 객체를 추가
	- `Child.prototype.prototype == Parent.prototype`


9. prototype 활용 상속

		> function Parent() { this.name = "Father"; }
		> Parent.prototype.getName = function() { 
		    return this.name; 
		  }
		> function object(p) {
		    var C = function(){}; 
		    C.prototype = p;
		    return new C();
		  }
		> var c = object( new Parent() )
		> c.name
		'Father'
		> c.getName()
		'Father'
		> var c1 = object( Parent.prototype)
		> c1.name
		undefined
		> c1.getName()
		undefined
		> c1.name = "childish"
		'childish'
		> c1.getName()
		'childish'

	* ES5의 경우 다음과 같이 활용 가능

			Object.create(p)	// 객체를 새로 생성하고 p를 prototype객체로 지정함

10. deep/shallow copy

	* prototype과는 관계없는 프로퍼티의 이야기다

11. mix-in

	* 프로퍼티 복사하기에 지나지 않는다.
	* 여러 객체의 프로퍼티를 복사하는 방식으로 이뤄짐

12. 메서드 빌려 쓰기

	- 배열 메서드 빌려쓰기 유용함. arguments에서 배열 메서드 활용

			var args = [].slice.call(arguments, 1, 3);
			

## 생성자 포인터 설정

- prototype.constructor
- 거의 정보 제공의 용도로만 사용

javascript
